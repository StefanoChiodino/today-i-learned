If I flip a coin and look at the result in secret, being heads, then the chances that it's heads it's 100% for me, but 50% for everybody else.

Source: Rationality Course

Via: Harvard

URL: 32 minutes in https://harvard.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=a780e65f-e1d0-47a2-97c4-ab3b0140edfb